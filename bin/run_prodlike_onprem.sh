#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$self_dir"/runfunc_onprem.sh

PIPELINECOMMIT=69465b54c7253313a8babf754db54d92806f4c69
CONF=prodlike.sh
PIPEGOALS_SCRAPE=""
PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_FINISH="upload-minio,upload-elk,make-report,send-report,import-mainlog,compress-files,upload-steplogs"
#PIPEGOALS_FINISH="make-report,send-report,compress-files"
SECRETS="$self_dir"/../vaultsecrets/test
DEVOPSFLAGS="hostdeps checkout pull build minio_copy"
#DEVOPSFLAGS="hostdeps checkout pull build"
POSTRUN=""

SSH_CONFIG="$self_dir"/../vaultsecrets/test/servers/config
SSH_HOST="onprem-test"

# make relative paths in sshconfig work
cd "$self_dir"/..

launch
