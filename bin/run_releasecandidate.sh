#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$self_dir"/runfunc.sh

PIPELINECOMMIT=e032fb9
CONF=releasecandidate.sh
PIPEGOALS_SCRAPE=""
PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_FINISH="upload-minio,upload-elk,make-report,send-report,import-mainlog,compress-files,upload-steplogs"
SECRETS="$self_dir"/../secrets/test/gitlab-deploy/secrets.sh
DEVOPSFLAGS="hostdeps checkout pull build minio_copy"
POSTRUN=""

launch
