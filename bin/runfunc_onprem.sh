#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
set -x

function launch() {
    local _PIPELINECOMMIT="${PIPELINECOMMIT:-unset}"
    local _CONF="${CONF:-unset}"
    local _PIPEGOALS_SCRAPE="${PIPEGOALS_SCRAPE:-}"
    local _PIPEGOALS_PROCESS="${PIPEGOALS_PROCESS:-}"
    local _PIPEGOALS_FINISH="${PIPEGOALS_FINISH:-}"
    local _SECRETS="${SECRETS:-$self_dir/../vaultsecrets/test}"
    local _DEVOPSFLAGS="${DEVOPSFLAGS:-hostdeps checkout pull build}"
    local _POSTRUN="${POSTRUN:-true}"
    local _SSH_CONFIG="${SSH_CONFIG:-}"
    local _SSH_HOST="${SSH_HOST:-}"

    local TMPF=$(mktemp)
    echo '#!/bin/bash \
             rm -rf /var/tmp/pipeline_deploy /var/tmp/outputdir devops ops.sh '"$_CONF"' common_settings.sh common_settings_interpolate.sh && \
             grep -q proxy /etc/environment || ( \
               echo "export HTTP_PROXY=http://prodproxy.wdmz.ams.se:8080" | sudo tee -a /etc/environment && \
               echo "export HTTPS_PROXY=http://prodproxy.wdmz.ams.se:8080" | sudo tee -a /etc/environment && \
               echo "export https_proxy=http://prodproxy.wdmz.ams.se:8080" | sudo tee -a /etc/environment && \
               echo "export http_proxy=http://prodproxy.wdmz.ams.se:8080" | sudo tee -a /etc/environment && \
               echo "export no_proxy=gitlab.jobtechdev.se,jobtechdev.se,minio.arbetsformedlingen.se,arbetsformedlingen.se" | sudo tee -a /etc/environment && \
               echo "export NO_PROXY=gitlab.jobtechdev.se,jobtechdev.se,minio.arbetsformedlingen.se,arbetsformedlingen.se" | sudo tee -a /etc/environment ; ) && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/bin/devops && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/ops.sh && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/'"$_CONF"' && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/common_settings.sh && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/common_settings_interpolate.sh && \
             unset -f which && \
             sudo setenforce 0 && \
             . /etc/environment && \
	     sudo dnf install -y podman && \
             bash devops --secretsdir "$PWD/secrets" --deploydir /var/tmp/pipeline_deploy --commit '"$_PIPELINECOMMIT"' --conf ops.sh '"$_DEVOPSFLAGS"' && \
             cd /var/tmp/pipeline_deploy && \
             for G in '"$_PIPEGOALS_SCRAPE" "$_PIPEGOALS_PROCESS" "$_PIPEGOALS_FINISH"'; do \
                bin/pipeline_group --conf $HOME/'"$_CONF"' --groups $G -- --ldir /var/tmp/outputdir; \
             done; '"$_POSTRUN" >"$TMPF"

    ssh -F "$_SSH_CONFIG" "$_SSH_HOST" mkdir -p secrets

    scp -F "$_SSH_CONFIG" -r "$_SECRETS" "$_SSH_HOST":"secrets/"

    chmod a+rx "$TMPF"
    scp -F "$_SSH_CONFIG" "$TMPF" "$_SSH_HOST":job.sh

    ssh -F "$_SSH_CONFIG" "$_SSH_HOST" "nohup bash job.sh >out.log 2>err.log </dev/null &"
    rm -f "$TMPF"
}
