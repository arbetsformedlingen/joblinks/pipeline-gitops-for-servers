#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

PIPELINECOMMIT=97bd67b

CONF=prod.sh

PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_FINISH="upload-minio,upload-elk,make-report,send-report,import-mainlog,compress-files,upload-steplogs"

launchotron --instance-type c5n.2xlarge \
            --async \
            -u "$self_dir"/../secrets/test/gitlab-deploy/secrets.sh \
            '. secrets.sh && \
             rm -f secrets.sh && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$PIPELINECOMMIT'/bin/devops && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$PIPELINECOMMIT'/conf/ops.sh && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$PIPELINECOMMIT'/conf/'"$CONF"' && \
             bash devops --deploydir /tmp/pipeline_deploy --commit '"$PIPELINECOMMIT"' --conf ops.sh --secretsbranch master --closedsource hostdeps checkout pull build && \
             cd /tmp/pipeline_deploy && \
             for G in '"$PIPEGOALS_PROCESS" "$PIPEGOALS_FINISH"'; do \
                bin/pipeline_group --conf $HOME/'"$CONF"' --groups $G -- --ldir /tmp/outputdir; \
             done; \
             sudo shutdown -h now'
