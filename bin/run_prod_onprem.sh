#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$self_dir"/runfunc_onprem.sh

PIPELINECOMMIT=69465b54c7253313a8babf754db54d92806f4c69
CONF=prod.sh
PIPEGOALS_SCRAPE="data-retrieval"
PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_FINISH="upload-minio,upload-elk,make-report,send-report,import-mainlog,compress-files,upload-steplogs"
SECRETS="$self_dir"/../vaultsecrets/prod
DEVOPSFLAGS="hostdeps checkout pull build"
POSTRUN=""

SSH_CONFIG="$self_dir"/../vaultsecrets/prod/servers/config
SSH_HOST="onprem-prod"

# make relative paths in sshconfig work
cd "$self_dir"/..

launch
