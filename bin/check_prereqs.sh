#!/usr/bin/env bash
set -eEu -o pipefail


# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"



SECRETSCONF=${1:-secrets/prod/servers/config}
CRONHOST=${2:-jobtechlinks-cron2}
JUMPHOST=${3:-}


# ssh config
ssh_conf_file="$script_dir/../$SECRETSCONF"


function bash_require() {
    local req_maj=$1
    local req_min=$2

    local BV=(${BASH_VERSION//./ })
    if [[ ${BV[0]} -lt ${req_maj} ]] || ( [[ ${BV[0]} = ${req_maj} ]] && [[ ${BV[1]} -lt ${req_min} ]]); then
        return 1
    fi
    return 0
}

function verify_tools() {
    for tool in bash git make ssh; do
        command -v "${tool}" >/dev/null || { echo "*** no ${tool} found in PATH" >&2; exit 1; }
    done

    bash_require 3 0 || { echo "**** upgrade bash to >= 3.0"; exit 1; }
}


# ugh, things are getting hairy, as some devs use macs
function os_specific_checks() {
    command -v "make" >/dev/null || { echo "*** no make found in PATH" >&2; exit 1; }
    if ! make --version 2>/dev/null | grep -q GNU; then
        command -v "gnumake" >/dev/null || { echo "*** no gnumake found in PATH" >&2; exit 1; }
    fi
}



function check_connection() {
    echo "checking connection to $1 ..." >&2
    if ! ssh -F "$ssh_conf_file" "$1" true; then
        echo "cannot connect to host $1" >&2; exit 1
    fi
}


verify_tools

os_specific_checks

if [ -n "$JUMPHOST" ]; then
    check_connection "$JUMPHOST" || exit 1
fi

check_connection "$CRONHOST" || exit 1

echo "Kit looks good." >&2
