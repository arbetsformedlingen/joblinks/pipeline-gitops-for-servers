#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$self_dir"/runfunc_onprem.sh

PIPELINECOMMIT=895bcf2

CONF=test-small-without-scrape.sh
PIPEGOALS_SCRAPE=""

#CONF=test-small-with-scrape.sh
#PIPEGOALS_SCRAPE="data-retrieval"

PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_FINISH="upload-minio,upload-elk,make-report,send-report,import-mainlog,compress-files,upload-steplogs"
SECRETS="$self_dir"/../vaultsecrets/test
DEVOPSFLAGS="hostdeps checkout pull build minio_copy"
POSTRUN=""

SSH_CONFIG="$SECRETS"/servers/config
SSH_HOST="onprem-test"

# make relative paths in sshconfig work
cd "$self_dir"/..

launch
