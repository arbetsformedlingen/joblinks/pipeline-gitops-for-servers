#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


function launch() {
    local _PIPELINECOMMIT="${PIPELINECOMMIT:-unset}"
    local _CONF="${CONF:-unset}"
    local _PIPEGOALS_SCRAPE="${PIPEGOALS_SCRAPE:-}"
    local _PIPEGOALS_PROCESS="${PIPEGOALS_PROCESS:-}"
    local _PIPEGOALS_FINISH="${PIPEGOALS_FINISH:-}"
    local _SECRETS="${SECRETS:-$self_dir/../vaultsecrets/test}"
    local _DEVOPSFLAGS="${DEVOPSFLAGS:---closedsource hostdeps checkout pull build}"
    local _POSTRUN="${POSTRUN:-true}"

    local tmpf=$(mktemp)
    local secrets_tar="$tmpf".tgz
    local secret_parent=$(basename $(dirname "$_SECRETS"))
    trap "rm -f $tmpf $secrets_tar" EXIT
    tar -C "$_SECRETS/../.."  -zcf "$secrets_tar" --transform "s/$secret_parent/secrets/" "$secret_parent"

    launchotron --instance-type c5n.2xlarge \
            --async \
            --image ami-0581444235c0e70a2 \
            -u "$secrets_tar" \
            'tar -zxf '$(basename $secrets_tar)' && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/bin/devops && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/ops.sh && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/'"$_CONF"' && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/common_settings.sh && \
             curl -O https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/'$_PIPELINECOMMIT'/conf/common_settings_interpolate.sh && \
             sudo setenforce 0 &&\
             bash devops --secretsdir "$PWD/secrets" --deploydir /var/tmp/pipeline_deploy --commit '"$_PIPELINECOMMIT"' --conf ops.sh '"$_DEVOPSFLAGS"' && \
             cd /var/tmp/pipeline_deploy && \
             for G in '"$_PIPEGOALS_SCRAPE" "$_PIPEGOALS_PROCESS" "$_PIPEGOALS_FINISH"'; do \
                bin/pipeline_group --conf $HOME/'"$_CONF"' --groups $G -- --ldir /var/tmp/outputdir; \
             done; '"$_POSTRUN"
}
