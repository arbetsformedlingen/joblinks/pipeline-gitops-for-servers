#!/usr/bin/env bash
set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. "$self_dir"/runfunc.sh

PIPELINECOMMIT=5ebc92d4
CONF=prod.sh
PIPEGOALS_SCRAPE="data-retrieval"
PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_FINISH="upload-minio,upload-elk,make-report,send-report,import-mainlog,compress-files,upload-steplogs"
SECRETS="$self_dir"/../secrets/prod/gitlab-deploy/secrets.sh
DEVOPSFLAGS="--closedsource hostdeps checkout pull build"
POSTRUN="sudo shutdown -h now"

launch
