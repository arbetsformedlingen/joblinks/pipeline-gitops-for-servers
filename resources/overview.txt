
The deployment process:

+-------------------------------------------------+
| your laptop:                                    |
|   - use this repo (pipeline-gitops-for-servers) |
|   - git commit && git push                      |
|   - make deploy                                 |
+-------------------------------------------------+
                   ||
                   ||
                   VV
+------------------------------------------------------------+
| cron-server:                                               |
|   - crontab and configuration scripts are updated from git |
+------------------------------------------------------------+







The nightly batch processing:

+--------------------------------------------------------+
| cron-server:                                           |
|   - at the specified time, launch the specified script |
+--------------------------------------------------------+
               [launchotron]
                   ||
                   ||
                   VV
+---------------------------------------------------------+
| Just in time created compute instance:                  |
|   - run the steps specified in the configuration script |
|     - includes uploading every created file to minio    |
|     - includes importing result to Narval's elastic     |
|   - destroy compute instance when finished              |
+---------------------------------------------------------+
