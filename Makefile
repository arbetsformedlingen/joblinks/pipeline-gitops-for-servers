SSHCONF = conf/ssh.conf
ENVS    = $(shell . conf/env.sh ; echo $$envs)

## Determine which git branch is currently in use
BRANCH  = $(shell LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)


CRONHOST    = jobtechlinks-cron3
SECRETSCONF = vaultsecrets/test/servers/config
CRONFILE    = conf/cron_pipeline_test


deploy: | check_prereqs check_git upgrade_versions upgrade_cron


upgrade_versions:
	ssh -F $(SECRETSCONF) $(CRONHOST) "cd pipeline-gitops-for-servers && git pull"


upgrade_cron:
	ssh -F $(SECRETSCONF) $(CRONHOST) "sudo tee /etc/cron.d/pipeline" < conf/cron_pipeline


check_prereqs: secrets
	#@bin/check_prereqs.sh vaultsecrets/prod/servers/config jobtechlinks-cron2 jobtechlinks-jumphost2
	@bin/check_prereqs.sh $(SECRETSCONF) $(CRONHOST)


check_git:
	@if [ ! -z "$$(git status --porcelain)" ]; then \
		echo "**** error: repo is not clean" ;\
		exit 1 ;\
	fi
	@if [ "$(BRANCH)" != "master" ]; then \
		echo "**** error: not on master ($(BRANCH))" ;\
		exit 1 ;\
	fi
	git pull


secrets:
	@if [ ! -a secrets ]; then
	    echo "**** checkout the secrets repo in the project root, see README" >&1
	    exit 1
	fi
	#@if [ $$(git branch --show-current ) != $$( cd secrets; git branch --show-current; ) ]; then
	#    echo "**** main repo and secrets repo do not have matching branch names" >&1
	#    exit 1
	#fi


show_server_time:
	ssh -F $(SECRETSCONF) $(CRONHOST) date


sync_secrets:
	ssh -F $(SECRETSCONF) $(CRONHOST) "rm -rf pipeline-gitops-for-servers/vaultsecrets/prod pipeline-gitops-for-servers/vaultsecrets/test"
	#tar -C secrets -zcf - prod test | ssh -F $(SECRETSCONF) $(CRONHOST) "tar -C pipeline-gitops-for-servers/vaultsecrets -zxf -"
	echo SKIPPING SYNKING PROD SECRETS FOR NOW
	tar -C secrets -zcf - test | ssh -F $(SECRETSCONF) $(CRONHOST) "tar -C pipeline-gitops-for-servers/vaultsecrets -zxf -"



list_files_scraping:
	mc --config-dir vaultsecrets/prod/dot_mc ls scrapinghub/scrapinghub


list_files_pipeline:
	mc --config-dir vaultsecrets/prod/dot_mc ls pipeline/pipeline/pipeline
